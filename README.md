[![Build Status](https://gitlab.com/nickstone/ta-trend_imsva/badges/master/pipeline.svg?style=flat-square)](https://gitlab.com/nickstone/ta-trend_imsva/commits/master)



# TA-trend_imsva

Trend InterScan Messaging Security (IMSVA) Technology Add-on (TA) for Splunk provides knowledge object extractions and resolution lookups for IMSVA Message Trace, Policy Enforcement and ERS.

This add-on also provides CIM-compliant knowledge for use with Splunk Premium and CIM-based apps.
